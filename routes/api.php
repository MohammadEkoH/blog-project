<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//route api kategori
Route::get('/kategori/{id}', 'Admin\KategoriController@data_api_kategori')->name('data-kategori');
//route api sub kategori
Route::get('/sub_kategori/{id}', 'Admin\SubKategoriController@data_api_sub_kategori')->name('data-sub-kategori');
//routre api produk
Route::get('/produk/{id}', 'Admin\ProdukController@data_api_produk')->name('data-produk');
