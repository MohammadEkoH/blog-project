<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* ------------------- Admin ------------------- */

//auth
Route::auth();
Route::group(['middleware' => ['web','auth' ]], function(){
    //sub kategori
     Route::get('dashboard', function () {
        return view('admin/dashboard');
    });
    Route::get('/changePassword','Admin\AdminController@showChangePasswordForm');
    Route::post('/changePassword','Admin\AdminController@changePassword')->name('changePassword');
    Route::get('sub_kategori/cari','Admin\SubKategoriController@cari');
    Route::get('dashboard/sub_kategori', array('as' => 'sub_kategori.index', 'uses' => 'Admin\SubKategoriController@index'));
    Route::post('sub_kategori/store', array('as' => 'sub_kategori.store', 'uses' => 'Admin\SubKategoriController@store_sub_kategori'));
    Route::post('sub_kategori/update/{id}', array('as' => 'sub_kategori.update', 'uses' => 'Admin\SubKategoriController@update_sub_kategori'));
    Route::delete('sub_kategori/destroy/{id}', array('as' => 'sub_kategori.destroy', 'uses' => 'Admin\SubKategoriController@destroy'));

    //kategori
    Route::get('kategori/cari','Admin\KategoriController@cari');
    Route::get('dashboard/kategori', array('as' => 'kategori.index', 'uses' => 'Admin\KategoriController@index'));
    Route::post('kategori/store', array('as' => 'kategori.store', 'uses' => 'Admin\KategoriController@store_kategori'));
    Route::post('kategori/update/{id}', array('as' => 'kategori.update', 'uses' => 'Admin\KategoriController@update_kategori'));
    Route::delete('kategori/destroy/{id}', array('as' => 'kategori.destroy', 'uses' => 'Admin\KategoriController@destroy_kategori'));

    //produk
    Route::get('produk/cari','Admin\ProdukController@cari');
    Route::get('dashboard/produk', array('as' => 'produk.index', 'uses' => 'Admin\ProdukController@index'));
    Route::get('produk/create', array('as' => 'produk.create', 'uses' => 'Admin\ProdukController@create'));
    Route::get('produk/edit/{id}', array('as' => 'produk.edit', 'uses' => 'Admin\ProdukController@edit_produk'));
    Route::post('produk/store', array('as' => 'produk.store', 'uses' => 'Admin\ProdukController@store_produk'));
    Route::post('produk/update/{id}', array('as' => 'produk.update', 'uses' => 'Admin\ProdukController@update_produk'));
    Route::delete('produk/destroy/{id}', array('as' => 'produk.destroy', 'uses' => 'Admin\ProdukController@destroy_produk'));

    //konten
    Route::get('konten/cari','Admin\KontenController@cari');
    Route::get('dashboard/konten', array('as' => 'konten.index', 'uses' => 'Admin\KontenController@index'));
    Route::post('konten/store', array('as' => 'konten.store', 'uses' => 'Admin\KontenController@store'));

    //kontak
    Route::get('dashboard/kontak', array('as' => 'kontak.index', 'uses' => 'Admin\KontenController@kontak'));
    Route::post('kontak/store', array('as' => 'kontak.store', 'uses' => 'Admin\KontenController@store_kontak'));

    //kontak
    Route::get('dashboard/tentang', array('as' => 'tentang.index', 'uses' => 'Admin\KontenController@tentang'));
    Route::post('tentang/store', array('as' => 'tentang.store', 'uses' => 'Admin\KontenController@store_tentang'));

});


/* ------------------- User ------------------- */

//route user
Route::get('/', array('as' => 'index', 'uses' => 'User\HomeController@index'));
Route::get('u/p','User\ProdukController@cari_produk');

//route user tim
Route::get('/u/detail', function () {
    return view('user/detail');
});

//route user kategori
Route::get('u/kategori', array('as' => 'kategori.KategoriProduk', 'uses' => 'User\ProdukController@KategoriProduk'));
Route::get('u/k','User\ProdukController@cari_kategori');

//route user detail
Route::get('u/detail', array('as' => 'detail.DetailProduk', 'uses' => 'User\ProdukController@DetailProduk'));

//route user kontak
Route::get('u/kontak', array('as' => 'kontak.Kontak', 'uses' => 'User\HomeController@Kontak'));

//route user tentang
Route::get('u/tentang', array('as' => 'tentang.Tentang', 'uses' => 'User\HomeController@Tentang'));

//route user send mail
Route::get('u/send', array('as' => 'send.sendFeedback', 'uses' => 'User\HomeController@sendFeedback'));
