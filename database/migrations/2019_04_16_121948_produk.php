<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Produk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id_produk', true);
            $table->integer('sub_kategori_id')->unsigned();
            $table->string('gambar1', 50);
            $table->string('gambar2', 50);
            $table->string('gambar3', 50);
            $table->string('gambar4', 50);
            $table->string('nama', 100);
            $table->string('harga', 30);
            $table->text('deskripsi');
            $table->timestamps();

        });

        Schema::table('products', function (Blueprint $table) {
            $table->foreign('sub_kategori_id')->references('id_sub_kategori')->on('sub_kategori')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
