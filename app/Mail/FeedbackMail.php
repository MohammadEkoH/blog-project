<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Kategori;
use App\Konten;
use App\Kontak;

class FeedbackMail extends Mailable
{
    use Queueable, SerializesModels;  
    public $emailFrom, $nama, $pesan;

    /**
     * Create a new message instance.
     *
     * @return void
     */ 
    public function __construct($emailFrom, $nama, $pesan)
    { 
        $this->emailFrom = $emailFrom;
        $this->nama = $nama;
        $this->pesan = $pesan;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {  
        $konten = Konten::all()->first();
        $data['title'] = $konten['title']; 

        return $this->view('user.email_pesan', $data);
    }
}
