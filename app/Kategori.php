<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table = "kategori";
    public $timestamps = true;
    protected $primaryKey = 'id_kategori';

    protected $fillable = array('id_kategori','nama');

    public function sub_kategori(){
        return $this->belongsTo('App\SubKategori', 'id_sub_kategori');
    }

    public function produk(){
        return $this->hasMany('App\Produk', 'id_produk');
    }

}
