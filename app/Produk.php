<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    protected $table = "products";
    public $timestamps = true;
    protected $primaryKey = 'id_produk';

    protected $fillable = array('id_produk','nama', 'harga','sub_kategori_id', 'deskripsi', 'gambar1', 'gambar2', 'gambar3', 'gambar4');

    public function sub_kategori(){
        return $this->belongsTo('App\SubKategori', 'sub_kategori_id', 'id_sub_kategori');
    }

}
