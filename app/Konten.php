<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Konten extends Model
{
    protected $table = "konten";
    public $timestamps = true;
    protected $primaryKey = 'id_konten';

    protected $fillable = array('id_konten','background', 'title', 'deskripsi', 'logo');
}
