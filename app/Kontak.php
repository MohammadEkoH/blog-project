<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kontak extends Model
{
    protected $table = "kontak";
    public $timestamps = true;
    protected $primaryKey = 'id_kontak';

    protected $fillable = array('id_kontak','alamat', 'nomor', 'email', 'jam_kerja');
}
