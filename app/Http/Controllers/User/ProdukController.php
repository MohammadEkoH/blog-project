<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Validator;
use DB;
use App\Produk;
use App\Kategori; 
use App\Kontak;
use App\Konten;
use App\Http\Requests;
use App\Admin as Admin;

class ProdukController extends Controller
{
    public function index(){
        $kategori = Kategori::orderBy('id')->get();
        $data = array(  
            'data_terbaru'  => Produk::paginate(5)->sortByDesc('created_at'),
            'data_kategori' => Produk::select(DB::raw('kategori_id, count(id) as total'))
                        ->groupby('kategori_id')
                        ->orderby('kategori_id','asc')
                        ->get()
        );
        return view('user.produk',$data)->with('list_kategori', $kategori);
    }

    public function cari_produk(Request $request)
    { 
        $cari = '';
        $cari = $request->cari;
        $data = array(  
            'list_kategori'         => Kategori::orderBy('id_kategori', 'desc')->get(), 
            'data_terbaru'  => Produk::paginate(5)->sortByDesc('created_at'), 
            'data_produk' => Produk::orderBy('id_produk')
                                ->where('nama','like',"%".$cari."%")
                                ->paginate()
        );

        $kontak = Kontak::all()->first(); 
        $data['alamat'] = $kontak['alamat'];
        $data['nomor'] = $kontak['nomor'];
        $data['email'] = $kontak['email'];

        $konten = Konten::all()->first();
        $data['title'] = $konten['title'];
        $data['logo'] = $konten['logo'];
        $data['background'] = $konten['background'];

        return view('user.home', $data);
    }

    public function KategoriProduk(){
        $id = Input::get('id') ;
        $data = array ( 
            'where_data_kategori'   => Produk::where('sub_kategori_id', $id)->paginate(16), 
            'list_kategori'         => Kategori::orderBy('id_kategori', 'desc')->get(), 
            'data_terbaru'          => Produk::orderBy('id_produk', 'desc')->limit(3)->get(),
            'data_kategori'         => Produk::select(DB::raw('sub_kategori_id, count(id_produk) as total'))
                                    ->groupby('sub_kategori_id')
                                    ->orderby('sub_kategori_id','asc')
                                    ->get() 
        );

        $data['where_data_kategori']->setPath('kategori?id='. $id);

        $kontak = Kontak::all()->first(); 
        $data['alamat'] = $kontak['alamat'];
        $data['nomor'] = $kontak['nomor'];
        $data['email'] = $kontak['email'];

        $konten = Konten::all()->first();
        $data['title'] = $konten['title'];
        $data['logo'] = $konten['logo'];
        $data['background'] = $konten['background'];

        return view('user.kategori',$data);
    }

    public function cari_kategori(Request $request)
    { 
        $cari = '';
        $cari = $request->cari;
        $data = array(  
            'list_kategori' => Kategori::orderBy('id_kategori', 'desc')->get(), 
            'data_terbaru'  => Produk::paginate(16)->sortByDesc('created_at'), 
            'data_kategori' => Kategori::orderBy('id_kategori')
                                ->where('nama','like',"%".$cari."%")
                                ->paginate()
        );

        $kontak = Kontak::all()->first(); 
        $data['alamat'] = $kontak['alamat'];
        $data['nomor'] = $kontak['nomor'];
        $data['email'] = $kontak['email'];

        $konten = Konten::all()->first();
        $data['title'] = $konten['title'];
        $data['logo'] = $konten['logo'];
        $data['background'] = $konten['background'];

        return view('user.home', $data);
    }

    public function DetailProduk(){
        $id = Input::get('id') ;
        $data2 = array (  
            'data_terbaru'  => Produk::orderBy('created_at', 'desc')->limit(3)->get(),
            'list_kategori' => Kategori::orderBy('id_kategori', 'desc')->get(),
            'data_kategori' => Produk::select(DB::raw('sub_kategori_id, count(id_produk) as total'))
                                    ->groupby('sub_kategori_id')
                                    ->orderby('sub_kategori_id','asc')
                                    ->get() 
        );

        $data = array(  
            'where_data'    => Produk::where('id_produk', $id)->get()->first()
        ); 

        foreach ($data as $row) { 
            $data['id']         = $row['id_produk']; 
            $data['nama']       = $row['nama']; 
            $data['harga']      = $row['harga']; 
            $data['gambar1']    = $row['gambar1']; 
            $data['gambar2']    = $row['gambar2']; 
            $data['gambar3']    = $row['gambar3']; 
            $data['gambar4']    = $row['gambar4']; 
            $data['deskripsi']  = $row['deskripsi']; 
            $data['sub_kategori'] = $row->sub_kategori['nama'];
            $data['kategori']   = $row->sub_kategori->kategori['nama'];
        }
 
        $kontak = Kontak::all()->first(); 
        $data2['alamat'] = $kontak['alamat'];
        $data2['nomor'] = $kontak['nomor'];
        $data2['email'] = $kontak['email'];

        $konten = Konten::all()->first();
        $data['title'] = $konten['title'];
        $data['logo'] = $konten['logo'];
        $data['background'] = $konten['background'];

        return view('user.detail',$data)->with($data2); 
    } 
}
