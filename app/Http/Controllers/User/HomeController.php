<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Validator;
use DB;
use App\Produk;
use App\Kategori;
use App\Konten; 
use App\Http\Requests;
use App\Admin as Admin;
use App\SubKategori;
use App\Kontak;
use Illuminate\Support\Facades\Mail;
use App\Mail\FeedbackMail;
  
class HomeController extends Controller
{
    public function index(){ 
        $data = array(  
            'data_produk'       => Produk::orderBy('created_at', 'desc')->paginate(16),
            'list_kategori'     => Kategori::orderBy('id_kategori', 'desc')->get(),
            'data_terbaru'      => Produk::orderBy('created_at', 'desc')->limit(3)->get()
        );   

        $kontak = Kontak::all()->first(); 
        $data['alamat'] = $kontak['alamat'];
        $data['nomor'] = $kontak['nomor'];
        $data['email'] = $kontak['email'];

        $konten = Konten::all()->first();
        $data['title'] = $konten['title'];
        $data['logo'] = $konten['logo'];
        $data['background'] = $konten['background']; 
 
        return view('user.home', $data); 
    }  
    
    public function Kontak(){ 
        $data = array (  
            'list_kategori' => Kategori::orderBy('id_kategori', 'desc')->get(),
            'jam_kerja'     => Kontak::get()
        );

        $kontak = Kontak::all()->first(); 
        $data['alamat'] = $kontak['alamat'];
        $data['nomor'] = $kontak['nomor'];
        $data['email'] = $kontak['email'];

        $konten = Konten::all()->first();
        $data['title'] = $konten['title'];
        $data['logo'] = $konten['logo'];
        $data['background'] = $konten['background'];

        return view('user.kontak', $data);
    }

    public function Tentang(){
        $data = array ( 
            'list_kategori'     => Kategori::orderBy('id_kategori', 'desc')->get(), 
            'deskripsi_konten'  => Konten::all()->first(),
            'data_terbaru'      => Produk::orderBy('id_produk', 'desc')->limit(3)->get(),
            'data_kategori'     => Produk::select(DB::raw('sub_kategori_id, count(id_produk) as total'))
                                    ->groupby('sub_kategori_id')
                                    ->orderby('sub_kategori_id','asc')
                                    ->get() 
        );  

        $kontak = Kontak::all()->first(); 
        $data['alamat'] = $kontak['alamat'];
        $data['nomor'] = $kontak['nomor'];
        $data['email'] = $kontak['email'];

        $konten = Konten::all()->first();
        $data['title'] = $konten['title'];
        $data['logo'] = $konten['logo'];
        $data['background'] = $konten['background'];

        return view('user.tentang', $data);
    } 

    public function sendFeedback()
    {    
        $kontak     = Kontak::all()->first();  
        $emailTo    = $kontak['email']; 
        $emailFrom  = Input::post('email');  
        $nama       = Input::post('nama');         
        $pesan      = Input::post('pesan');  
         
        Mail::to($emailTo, 'The Start')->send(new FeedbackMail($emailFrom, $nama, $pesan));
        
        $data = array (  
            'list_kategori' => Kategori::orderBy('id_kategori', 'desc')->get(),
            'jam_kerja'     => Kontak::get()
        );
 
        return Redirect::action('User\HomeController@Kontak')
                ->with('success','Pesan berhasil dikirim');
    }
}
