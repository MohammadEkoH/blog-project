<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Validator;
use DB;
use App\Konten;
use Intervention\Image\ImageManagerStatic as Image;
use App\Kontak;
use App\Http\Requests;
use App\Admin as Admin;

class KontenController extends Controller
{
    //menampilkan data program
    public function index(){
        $data = array( 'data_konten' => Konten::orderBy('id_konten')->get()->first());
        foreach($data as $row){
            $data['id']         = $row['id_konten'];
            $data['title']      = $row['title'];
            $data['background'] = $row['background'];
            $data['logo']      = $row['logo'];
        }

        return view('admin.konten' ,$data);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title'         => 'required|max:100',
            'background'    => 'file|max:2000',
            'logo'          => 'file|max:2000'
        ]);
        $id = Input::post('id');

        $data = Konten::all()->first();

        if ($data == null) {
            $tambah = new Konten();
            $tambah->title          = $request['title'];

            $file       = $request->file('logo');
            $fileName   =  'daengweb-' . time() . '.' . $file->getClientOriginalName();
            $request->file('logo')->move("storage/files/", $fileName);
            // Disini proses mendapatkan judul dan memindahkan letak gambar ke folder image
            $file1       = $request->file('background');
            $fileName1   =  'daengweb-' . time() . '.' . $file1->getClientOriginalName();
            $request->file('background')->move("storage/files/", $fileName1);

            $tambah->logo = $fileName;
            $tambah->background = $fileName1;
            $tambah->save();
        }
        else
        {
            $update = Konten::where('id_konten', $id)->first();
            $update->title           = $request['title'];

            if($request->file('logo') == "")
            {
                $update->logo = $update->logo;
            }
            else
            {
                $path = public_path("storage/files/".$update->logo);
                if (file_exists($path)) {

                    unlink($path);
                }
                $file       = $request->file('logo');
                $fileName   = 'daengweb-' . time() . '.' .$file->getClientOriginalName();
                $request->file('logo')->move("storage/files/", $fileName);
                $update->logo = $fileName;
            }

            //bag
            if($request->file('background') == "")
            {
                $update->background = $update->background;
            }
            else
            {
                $path1 = public_path("storage/files/".$update->background);
                if (file_exists($path1)) {
                    unlink($path1);
                }
                $file1       = $request->file('background');
                $fileName1   = time() . '.' .$file1->getClientOriginalName();
                $request->file('background')->move("storage/files/", $fileName1);
                $update->background = $fileName1;
            }
            $update->update();
        }


        return redirect()
            ->back()
            ->with('success','Berhasil diubah !!.');

    }

     //menampilkan data program
     public function kontak(){
        $data = array( 'data_kontak' => Kontak::orderBy('id_kontak')->get()->first());
        foreach($data as $row){
            $data['id']         = $row['id_kontak'];
            $data['alamat']     = $row['alamat'];
            $data['nomor']      = $row['nomor'];
            $data['email']      = $row['email'];
            $data['jam_kerja']  = $row['jam_kerja'];
        }
        return view('admin.kontak', $data);
    }

    //simpan data Kategori
    public function store_kontak(Request $request){

        $this->validate($request, [
            'alamat'      => 'required|max:100',
            'nomor'       => 'required|digits_between:11,12|numeric',
            'jam_kerja'   => 'required|min:1',
            'email'       => 'required|max:100'
        ]);

        $id = Input::post('id');

        $data = Kontak::all()->first();

        if ($data == null) {
            $tambah = new Kontak();
            $tambah->alamat     = $request['alamat'];
            $tambah->email      = $request['email'];
            $tambah->nomor      = $request['nomor'];
            $tambah->jam_kerja  = $request['jam_kerja'];
            $tambah->save();
            $notif = 'Berhasil ditambah !!.';
        }else{
            $update             = Kontak::where('id_kontak', $id)->first();
            $update->alamat     = $request['alamat'];
            $update->email      = $request['email'];
            $update->nomor      = $request['nomor'];
            $update->jam_kerja  = $request['jam_kerja'];
            $update->update();
            $notif = 'Berhasil diupdate !!.';
        }

        return Redirect::action('Admin\KontenController@kontak')
            ->with('success', $notif);
    }

    //menampilkan data tentang
    public function tentang(){
        $data = array( 'data_tentang' => Konten::orderBy('id_konten')->get()->first());
        foreach($data as $row){
            $data['id']            = $row['id_konten'];
            $data['deskripsi']     = $row['deskripsi'];
        }
        return view('admin.tentang', $data);
    }

    public function store_tentang(Request $request){

        $this->validate($request, [
            'deskripsi'   => 'required|min:1'
        ]);

        $id = Input::post('id');

        $data = Konten::all()->first();

        if ($data == null) {
            $notif = 'Tambahkan data konten terlebih dahulu !!.';
        }else{
            $update             = Konten::where('id_konten', $id)->first();
            $update->deskripsi     = $request['deskripsi'];
            $update->update();
            $notif = 'Berhasil diupdate !!.';
        }

        return Redirect::action('Admin\KontenController@tentang')
            ->with('success', $notif);
    }
}

