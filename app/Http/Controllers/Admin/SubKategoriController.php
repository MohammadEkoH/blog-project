<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Validator;
use DB;
use App\Produk;
use App\Kategori;
use App\SubKategori;
use App\Gambar;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Requests;
use App\Admin as Admin;

class SubKategoriController extends Controller
{
    //menampilkan data program
    public function index(){

        $kategori = Kategori::orderBy('id_kategori')->get();
        $data = array(  'data_produk'       => Produk::orderBy('id_produk')->paginate(5),
                        'data_kategori'     => Kategori::orderBy('id_kategori')->get(),
                        'data_sub_kategori' => SubKategori::orderBy('id_sub_kategori')->paginate(15)
                    );

        return view('admin.sub_kategori',$data)->with('list_kategori', $kategori);
    }

    //api kategori
    public function data_api_sub_kategori($id)
    {
        $data = SubKategori::where('id_sub_kategori', $id)->get();

        return response()->json($data);
    }


    //simpan data sub kategori
    public function store_sub_kategori(Request $request){

        $this->validate($request, [
            'nama'          => 'required|max:100'
        ]);

        $sub_kategori = new SubKategori();
        $sub_kategori->nama         = $request->nama;
        $sub_kategori->kategori_id  = $request->kategori_id;

        $sub_kategori->save();

        return Redirect::action('Admin\SubKategoriController@index')
            ->with('success','Berhasil ditambah !!.');
    }


    public function update_sub_kategori(Request $request, $id)
    {
        $sub_kategori = SubKategori::find($id);
        $sub_kategori->nama         = $request->nama;
        $sub_kategori->kategori_id  = $request->kategori_id;
        $sub_kategori->save();

        return redirect()->back()
            ->with('success','Berhasil diubah !!.');
    }


    public function destroy($id_sub_kategori)
    {
        $sub_kategori = SubKategori::findOrFail($id_sub_kategori);
        $sub_kategori->delete();

        return redirect()->back()->with('success', 'Data Berhasil dihapus!!.');
    }

    public function cari(Request $request)
    {
        $kategori = Kategori::orderBy('id_kategori')->get();
        $cari = '';
        $cari = $request->cari;
        $data = array(  'crot'          => 0,
                        'data_sub_kategori' => SubKategori::orderBy('id_sub_kategori')
                                            ->where('nama','like',"%".$cari."%")
                                            ->paginate()
                    );

        return view('admin.sub_kategori',$data)->with('list_kategori', $kategori);
    }

}
