<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Validator;
use DB;
use App\Produk;
use App\Kategori;
use App\SubKategori;
use App\Gambar;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Requests;
use App\Admin as Admin;

class KategoriController extends Controller
{
    //menampilkan data program
    public function index(){

        $kategori = Kategori::orderBy('id_kategori')->get();
        $data = array(  'data_produk'       => Produk::orderBy('id_produk')->paginate(5),
                        'data_kategori'     => Kategori::orderBy('id_kategori')->paginate(10),
                        'data_sub_kategori' => SubKategori::orderBy('id_sub_kategori')->get()
                    );
        return view('admin.kategori',$data)->with('list_kategori', $kategori);
    }

    //api kategori
    public function data_api_kategori($id)
    {
        $data = Kategori::where('id_kategori', $id)->get();

        return response()->json($data);
    }

    //simpan data Kategori
    public function store_kategori(Request $request){

        $this->validate($request, [
            'nama'          => 'required|max:100'
        ]);

        $kategori = new Kategori();
        $kategori->nama      = $request->nama;

        $kategori->save();

        return Redirect::action('Admin\KategoriController@index')
            ->with('success','Berhasil ditambah !!.');
    }


    public function update_kategori(Request $request, $id)
    {
        $kategori = Kategori::find($id);
        $kategori->nama      = $request->nama;;
        $kategori->save();

        return redirect()->back()
            ->with('success','Berhasil diubah !!.');
    }

    public function destroy_kategori($id)
    {
        $kategori = Kategori::where('id_kategori', $id)->delete();

        return redirect()->back()->with('success', 'Data Berhasil dihapus!!.');
    }


    public function cari(Request $request)
    {
        $kategori = Kategori::orderBy('id_kategori')->get();
        $cari = '';
        $cari = $request->cari;
        $data = array(  'crot'          => 0,
                        'data_kategori' => Kategori::orderBy('id_kategori')
                                            ->where('nama','like',"%".$cari."%")
                                            ->paginate()
                    );

        return view('admin.kategori',$data)->with('list_kategori', $kategori);
    }

}
