<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Validator;
use DB;
use App\Produk;
use App\Kategori;
use App\SubKategori;
use App\Gambar;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Requests;
use App\Admin as Admin;

class ProdukController extends Controller
{
    //menampilkan data program
    public function index(){

        $kategori = Kategori::orderBy('id_kategori')->get();
        $data = array(  'data_produk'       => Produk::orderBy('id_produk')->paginate(5),
                        'data_kategori'     => Kategori::orderBy('id_kategori')->get(),
                        'data_sub_kategori' => SubKategori::orderBy('id_sub_kategori')->get()
                    );

        return view('admin.produk',$data)->with('list_kategori', $kategori);
    }

    //api produk
    public function data_api_produk($id)
    {
        $data = Produk::where('id_produk', $id)->get();

        return response()->json($data);
    }

    public function create(){
        $sub_kategori = SubKategori::orderBy('id_sub_kategori')->get();
        return view('admin.create.produk')->with('list_sub_kategori', $sub_kategori);
    }

    //simpan data produk
    public function store_produk(Request $request){
        $this->validate($request, [
            'nama'                => 'required|max:100',
            'sub_kategori_id'     => 'required|max:20',
            'harga'               => 'required|min:1',
            'deskripsi'           => 'required',
            'gambar1'             => 'required',
            'gambar1.*'           => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'gambar2'             => 'required',
            'gambar2.*'           => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'gambar3'             => 'required',
            'gambar3.*'           => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'gambar4'             => 'required',
            'gambar4.*'           => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        $tambah = new Produk();
        $tambah->sub_kategori_id    = $request['sub_kategori_id'];
        $tambah->harga              = $request['harga'];
        $tambah->nama               = $request['nama'];
        $tambah->deskripsi          = $request['deskripsi'];

        // Disini proses mendapatkan judul dan memindahkan letak gambar ke folder image
        $file1       = $request->file('gambar1');
        $fileName1   =  'daengweb-' . time() . '.' . $file1->getClientOriginalName();
        $request->file('gambar1')->move("storage/files/", $fileName1);

        $file2       = $request->file('gambar2');
        $fileName2   =  'daengweb-' . time() . '.' . $file2->getClientOriginalName();
        $request->file('gambar2')->move("storage/files/", $fileName2);

        $file3       = $request->file('gambar3');
        $fileName3   =  'daengweb-' . time() . '.' . $file3->getClientOriginalName();
        $request->file('gambar3')->move("storage/files/", $fileName3);


        $file4       = $request->file('gambar4');
        $fileName4   =  'daengweb-' . time() . '.' . $file4->getClientOriginalName();
        $request->file('gambar4')->move("storage/files/", $fileName4);


        $tambah->gambar1 = $fileName1;
        $tambah->gambar2 = $fileName2;
        $tambah->gambar3 = $fileName3;
        $tambah->gambar4 = $fileName4;
        $tambah->save();

        return Redirect::action('Admin\ProdukController@index')
                                ->with('success','Berhasil ditambah !!.');
    }

    public function edit_produk($id){
        $sub_kategori = SubKategori::orderBy('id_sub_kategori')->get();
        $produk = array(
            'data_produk' => Produk::where('id_produk',$id)->get()
        );
        return view('admin.edit.produk', $produk)->with('list_sub_kategori', $sub_kategori);
    }

    public function update_produk(Request $request, $id)
    {
        $this->validate($request, [
            'nama'              => 'required|max:100',
            'sub_kategori_id'   => 'required|max:20',
            'harga'             => 'required|min:1',
            'deskripsi'         => 'required',
            'gambar1'            => 'file|max:2000'
        ]);

        $update = Produk::where('id_produk', $id)->first();
        $update->nama               = $request['nama'];
        $update->harga              = $request['harga'];
        $update->sub_kategori_id    = $request['sub_kategori_id'];
        $update->deskripsi          = $request['deskripsi'];

        //gmabar 1
        if($request->file('gambar1') == "")
        {
            $update->gambar1 = $update->gambar1;
        }
        else
        {
            $path1 = public_path("storage/files/".$update->gambar1);

            if (file_exists($path1)) {
                //Skkni::delete($skkni, $path);
                unlink($path1);
            }

            $file1       = $request->file('gambar1');
            $fileName1   = $file1->getClientOriginalName().'_'.date('Y:m:d');
            $request->file('gambar1')->move("storage/files/", $fileName1);
            $update->gambar1 = $fileName1;
        }

        //gambar 2
        if($request->file('gambar2') == "")
        {
            $update->gambar2 = $update->gambar2;
        }
        else
        {
            $path2 = public_path("storage/files/".$update->gambar2);

            if (file_exists($path2)) {
                //Skkni::delete($skkni, $path);
                unlink($path2);
            }

            $file2       = $request->file('gambar2');
            $fileName2   = $file->getClientOriginalName().'_'.date('Y:m:d');
            $request->file('gambar2')->move("storage/files/", $fileName2);
            $update->gambar2 = $fileName2;
        }
        //gambar 3
        if($request->file('gambar3') == "")
        {
            $update->gambar3 = $update->gambar3;
        }
        else
        {
            $path3 = public_path("storage/files/".$update->gambar3);

            if (file_exists($path3)) {
                //Skkni::delete($skkni, $path);
                unlink($path3);
            }

            $file3       = $request->file('gambar3');
            $fileName3   = $file3->getClientOriginalName().'_'.date('Y:m:d');
            $request->file('gambar3')->move("storage/files/", $fileName3);
            $update->gambar3 = $fileName3;
        }
        //gambar 4
        if($request->file('gambar4') == "")
        {
            $update->gambar4 = $update->gambar4;
        }
        else
        {
            $path4 = public_path("storage/files/".$update->gambar4);

            if (file_exists($path4)) {
                //Skkni::delete($skkni, $path);
                unlink($path4);
            }

            $file4       = $request->file('gambar4');
            $fileName4   = $file->getClientOriginalName().'_'.date('Y:m:d');
            $request->file('gambar4')->move("storage/files/", $fileName4);
            $update->gambar4 = $fileName4;
        }

        $update->update();

        return Redirect::action('Admin\ProdukController@index')
                                ->with('success','Berhasil diubah !!.');
    }

    public function destroy_produk($id)
    {
        $produk = Produk::findOrFail($id);
        $path = public_path("storage/files/".$produk->gambar1);
        $path1 = public_path("storage/files/".$produk->gambar2);
        $path2 = public_path("storage/files/".$produk->gambar3);
        $path3 = public_path("storage/files/".$produk->gambar4);

        if (file_exists($path)) {
            unlink($path);
        }
        if (file_exists($path1)) {
            unlink($path1);
        }
        if (file_exists($path2)) {
            unlink($path2);
        }
        if (file_exists($path3)) {
            unlink($path3);
        }
        $produk->delete();
        return redirect()->back()->with('success', 'Data Berhasil dihapus!!.');
    }

    public function cari(Request $request)
    {
        $kategori = Kategori::orderBy('id_kategori')->get();
        $cari = '';
        $cari = $request->cari;
        $data = array(  'crot'          => 0,
                        'data_produk'        => Produk::orderBy('id_produk')
                                            ->where('nama','like',"%".$cari."%")
                                            ->paginate(),
                        'data_kategori' => Kategori::orderBy('id_kategori')->get());

        return view('admin.produk',$data)->with('list_kategori', $kategori);
    }

}
