<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubKategori extends Model
{
    protected $table = "sub_kategori";
    public $timestamps = true;
    protected $primaryKey = 'id_sub_kategori';

    protected $fillable = array('id_sub_kategori', 'kategori_id', 'nama');

    public function produk(){
        return $this->hasMany('App\Produk', 'id_produk');
    }

    public function kategori(){
        return $this->belongsTo('App\Kategori', 'kategori_id', 'id_kategori');
    }
}
