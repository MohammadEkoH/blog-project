@extends('admin.app')

@section('title', 'Konten')

@section('content')
<div class="row">
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ url('dashboard') }}">Dashboard</a></li>
        <li class="active">Konten</li>
    </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">

                <ul>
                        @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>

                    @if(session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
            <div class="col-md-12">
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li><a href="{{ url('dashboard/konten') }}">Konten</a></li>
                    <li><a href="{{ url('dashboard/kontak') }}">Kontak</a></li>
                    <li class="active"><a href="{{ url('dashboard/tentang') }}">Tentang</a></li>
                    <li class="dropdown">
                </ul>
                <div class="tab-content">
                        <div>
                                <form enctype="multipart/form-data" action="{{ url('tentang/store') }}" method="post">
                                    <input type="hidden" name="_token" value="{{ csrf_token()}}">
                                    <input type="hidden" name="id" value="{{ $id }}">
                                    {{ method_field('post') }}
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="exampleDeskripsi">Tentang</label>
                                            <textarea id="exampleDeskripsi" name="deskripsi" class="textarea" placeholder="Place some text here"
                                            style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                                            {{ $deskripsi }}
                                            </textarea>
                                            <div class="help-block with-errors">{{ $errors->first('deskripsi') }}</div>
                                        </div>
                                    </div>
                                    <!-- /.box-body -->

                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-primary pull-right">Submit</button>
                                    </div>
                                </form>
                        </div>
                        <!-- /.tab-pane -->
                        </div>
@endsection
