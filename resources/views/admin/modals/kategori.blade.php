<div class="modal fade" id="kategori-modal">
    <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title text-semibold kategori-modal-title"></h3>
                </div>
            <div class="modal-body">
                <form action="{{ url('kategori/store')}}" method="post" enctype="multipart/form-data" class="form-kategori">
                    <input type="hidden" name="_token" value="{{ csrf_token()}}">
                    {{ method_field('post') }}
                        <div class="form-group">
                            <label class="col-form-label">Nama Kategori :</label>
                            <input type="text" name="nama" class="form-control nama" required>
                            <div class="help-block with-errors">{{ $errors->first('bidang') }}</div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary text-semibold">Simpan</button>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>
