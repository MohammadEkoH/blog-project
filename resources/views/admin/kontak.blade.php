@extends('admin.app')

@section('title', 'Kontak')

@section('content')
<div class="row">
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ url('dashboard') }}">Dashboard</a></li>
        <li class="active">Konten</li>
    </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
                <ul>
                    @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>

                @if(session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
            <div class="col-md-12">
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li ><a href="{{ url('dashboard/konten') }}">Konten</a></li>
                    <li class="active"><a href="{{ url('dashboard/kontak') }}">Kontak</a></li>
                    <li><a href="{{ url('dashboard/tentang') }}">Tentang</a></li>
                    <li class="dropdown">
                </ul>
                <div class="tab-content">
                    <div>
                        <form enctype="multipart/form-data" action="{{ url('kontak/store') }}" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token()}}">
                            <input type="hidden" name="id" value="{{ $id }}">
                                {{ method_field('post') }}
                                <div class="box-body">
                                    <label for="exampleInputKontak">Alamat</label>
                                    <input type="text" value="{{ $alamat }}" class="form-control" name="alamat" id="exampleInputKontak" placeholder="Contoh : Jl. Soekarno 10A"><br>
                                    <label for="exampleInputKontak">Kontak</label>
                                    <input type="text" value="{{ $nomor }}" class="form-control" name="nomor" id="exampleInputKontak" placeholder="Contoh : 082233418xxx"><br>
                                    <label for="exampleInputEmail">Email</label>
                                    <input type="email" value="{{ $email }}" class="form-control" name="email" id="exampleInputEmail" placeholder="Contoh : example@email.co.id"><br>
                                    <label for="exampleInputJamKerja">jam Kerja</label>
                                    <textarea id="exampleDeskripsi" name="jam_kerja" class="textarea" placeholder="Place some text here"
                                    style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                                    {{ $jam_kerja }}
                                    </textarea>
                                    <div class="help-block with-errors">{{ $errors->first('deskripsi') }}</div>
                                </div>
                                <div class="box-footer">
                                    <button type="submit" class="btn btn-primary pull-right">Submit</button>
                                </div>
                            </form>
                        </div>
                </div>
@endsection
