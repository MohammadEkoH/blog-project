@extends('admin.app')

@section('title', 'Konten')

@section('content')
<div class="row">
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ url('dashboard') }}">Dashboard</a></li>
        <li><a href="{{ url('dashboard/konten') }}">Konten</a></li>
        <li class="active">Tambah</li>
    </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box box-primary">
            <div class="box-header with-border">
            <h3 class="box-title">Konten</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form enctype="multipart/form-data" action="{{ url('konten/store') }}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token()}}">
                {{ method_field('post') }}
            <div class="box-body">
                <div class="form-group">
                    <label for="exampleInputTitle">Title</label>
                    <input type="text" class="form-control" name="nama" id="exampleInputTitle" placeholder="Enter title">
                    <div class="help-block with-errors">{{ $errors->first('deskripsi') }}</div>
                </div>
                <div class="form-group">
                    <label for="exampleInputFile">Logo</label><br>
                    <img src="http://placehold.it/100x100" id="showgambar" style="max-width:200px;max-height:200px;">
                    <input type="file" id="inputgambar" name="gambar">
                    <div class="help-block with-errors">{{ $errors->first('gambar') }}</div>
                </div>
                <div class="form-group">
                    <label for="exampleDeskripsi">Deskripsi</label>
                    <textarea id="exampleDeskripsi" name="deskripsi" class="textarea" placeholder="Place some text here"
                    style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">

                    </textarea>
                    <div class="help-block with-errors">{{ $errors->first('deskripsi') }}</div>
                </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{ url('dashboard/konten') }}" class="btn btn-link">Close</a>
            </div>
            </form>
        </div>
        <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="{{asset('js/materialize.min.js')}}"></script>
        <script type="text/javascript">
            (function($){
                $(function(){

                    $('.button-collapse').sideNav();

                }); // end of document ready
                })(jQuery); // end of jQuery name space
        </script>
        <script type="text/javascript">

            function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#showgambar').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#inputgambar").change(function () {
            readURL(this);
        });

        </script>
@endsection
