@extends('admin.app')

@section('title', 'Produk')

@section('content')
<div class="row">
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ url('dashboard') }}">Dashboard</a></li>
        <li><a href="{{ url('dashboard/produk') }}">Produk</a></li>
        <li class="active">Tambah</li>
    </ol>
    </section>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<body>
    <!-- Main content -->
    <section class="content">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Insert</h3>
            </div>


            @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif

            @foreach ($data_produk as $produk)

            <form method="post" action="{{url('produk/update', $produk->id_produk)}}" enctype="multipart/form-data">
            {{csrf_field()}}
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-form-label">Sub Kategori :</label>
                        <select class="form-control sub_kategori_id" name="sub_kategori_id">
                            @foreach ($list_sub_kategori as $key ):
                                <option value="{{$key->id_sub_kategori}}">{{ $key->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="nama-produk">Nama</label>
                        <input type="text" value="{{ $produk->nama }}" class="form-control" name="nama" id="nama-produk" placeholder="Masukkan Nama Produk">
                        <div class="help-block with-errors">{{ $errors->first('nama') }}</div>
                    </div>

                    <div class="form-group">
                        <label for="harga-produk">Harga</label>
                        <input type="text" value="{{ $produk->harga }}" class="form-control" name="harga" id="harga-produk" placeholder="Masukkan Harga Produk">
                        <div class="help-block with-errors">{{ $errors->first('harga') }}</div>
                    </div>

                    <div class="form-group">
                        <label for="gambar-produk">Gambar</label>
                        <div  class="col-md-12">
                            <div id="foto_1" class="col-md-3">
                                <img src="{{ asset('') }}storage/files/{{ $produk['gambar1'] }}" id="showgambar1" style="max-width:200px;max-height:200px;">
                                <input type="file" id="inputgambar1" name="gambar1">
                                <div class="help-block with-errors">{{ $errors->first('gambar1') }}</div>
                            </div>
                            <div id="foto_2" class="col-md-3">
                                <img src="{{ asset('') }}storage/files/{{ $produk['gambar2'] }}" id="showgambar2" style="max-width:200px;max-height:200px;">
                                <input type="file" id="inputgambar2" name="gambar2">
                                <div class="help-block with-errors">{{ $errors->first('gambar2') }}</div>
                            </div>
                            <div id="foto_3" class="col-md-3">
                                <img src="{{ asset('') }}storage/files/{{ $produk['gambar3'] }}" id="showgambar3" style="max-width:200px;max-height:200px;">
                                <input type="file" id="inputgambar3" name="gambar3">
                                <div class="help-block with-errors">{{ $errors->first('gambar3') }}</div>
                            </div>
                            <div id="foto_4" class="col-md-3">
                                <img src="{{ asset('') }}storage/files/{{ $produk['gambar4'] }}" id="showgambar4" style="max-width:200px;max-height:200px;">
                                <input type="file" id="inputgambar4" name="gambar4">
                                <div class="help-block with-errors">{{ $errors->first('gambar4') }}</div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                            <label for="deskripsi-produk">Deskripsi</label>
                            <textarea id="exampleDeskripsi" name="deskripsi" placeholder="Place some text here"
                            style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                            {{ $produk->deskripsi }}</textarea>
                            <div class="help-block with-errors">{{ $errors->first('deskripsi') }}</div>
                        </div>
                </div>

                    <div class="modal-footer">
                        <a type="button" href="{{ url('dashboard/produk') }}" class="btn btn-link">Close</a>
                        <button type="submit" class="btn btn-primary text-semibold">Simpan</button>
                    </div>
                </div>
            </form>
            @endforeach
        </div>
    </section>


<script type="text/javascript">


    $(document).ready(function() {

      $(".btn-success").click(function(){
          var html = $(".clone").html();
          $(".increment").after(html);
      });

      $("body").on("click",".btn-danger",function(){
          $(this).parents(".control-group").remove();
      });

    });

</script>

<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="{{asset('js/materialize.min.js')}}"></script>
<script type="text/javascript">
    (function($){
        $(function(){

            $('.button-collapse').sideNav();

        }); // end of document ready
        })(jQuery); // end of jQuery name space
</script>
<script type="text/javascript">

    function readURL1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#showgambar1').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#inputgambar1").change(function () {
        readURL1(this);
    });

    function readURL2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#showgambar2').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#inputgambar2").change(function () {
        readURL2(this);
    });

    function readURL3(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#showgambar3').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#inputgambar3").change(function () {
        readURL3(this);
    });

    function readURL4(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#showgambar4').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#inputgambar4").change(function () {
        readURL4(this);
    });

</script>

</div>

@endsection
