@extends('admin.app')

@section('title', 'Daftar Admin')

@section('content')
<div class="row">
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ url('dashboard') }}">Dashboard</a></li>
        <li class="active">Daftar Admin</li>
    </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <ul>
                @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>

            @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif

            <style type="text/css">
                .pagination li{
                    float: left;
                    list-style-type: none;
                    margin:5px;
                }
            </style>
            <div class="col-md-12">
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                    <div class="tab-content">
                        <div class="tab-pane active">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">
                                        <button type="button"  onclick="AddKategori()" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#kategori-modal">
                                            <i class="fa fa-plus-square"></i><b> Tambah</b>
                                        </button>
                                    </h3>
                                    <div class="box-tools">
                                        <form action="/kategori/cari" method="GET">
                                            <div class="input-group ">
                                                <input type="text" name="cari" class="form-control pull-right" value="{{ old('cari') }}" placeholder="Search">
                                                <div class="input-group-btn">
                                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body table-responsive no-padding">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th width="10%">No</th>
                                                <th>Nama</th>
                                                <th>Password</th>
                                                <th>Email</th>
                                                <th width="15%">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                                @php
                                                    $no = 1;
                                                @endphp
                                                @foreach ($users as $user)
                                                <tr>
                                                    <td>{{ $no }}</td>
                                                    <td>{{ $user->name }}</td>
                                                    <td>{{ $user->password }}</td>
                                                    <td>{{ $user->email }}</td>
                                                    <td>
                                                        <button type="button" data-toggle="modal" data-target="#kategori-modal" onclick="EditKategori('{{ $user->id }}')" class="btn btn-success btn-sm"><i class="fa fa-edit"></i> Edit</button>
                                                        <a href="javascript:;" data-toggle="modal" onclick="deleteKategori({{$user->id_kategori}})"
                                                                data-target="#DeleteModal" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Delete</a>
                                                    </td>
                                                    @php
                                                        $no++;
                                                    @endphp
                                                </tr>
                                                @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@include('admin.modals.delete')

<script type="text/javascript">

    function deleteKategori(id_kategori)
    {
        var id_kategori = id_kategori;
        var url = '{{ route("kategori.destroy", ":id_kategori") }}';
        url = url.replace(':id_kategori', id_kategori);
        $("#deleteForm").attr('action', url);
    }

    function formSubmit()
    {
        $("#deleteForm").submit();
    }

</script>
@endsection
