@extends('admin.app')

@section('title', 'Produk')

@section('content')
<div class="row">
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ url('dashboard') }}">Dashboard</a></li>
        <li class="active">Produk</li>
    </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
                <ul>
                    @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>

                @if(session('success'))
                  <div class="alert alert-success">
                      {{ session('success') }}
                  </div>
                @endif

                <style type="text/css">
                    .pagination li{
                        float: left;
                        list-style-type: none;
                        margin:5px;
                    }
                </style>
            <div class="col-md-12">
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                    <li class="active"><a href="{{ url('dashboard/produk') }}">Produk</a></li>
                    <li><a href="{{ url('dashboard/sub_kategori') }}">Sub Kategori</a></li>
                    <li><a href="{{ url('dashboard/kategori') }}">Kategori</a></li>
                    <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active">
                            <div class="box">
                                <div class="box-header">
                                    <div>
                                        <h3 class="box-title">
                                            <a href="{{ url('produk/create') }}" class="btn btn-primary btn-sm"><i class="fa fa-plus-square"></i> Tambah</a>
                                        </h3> <br><br>
                                        <div class="box-tools">
                                            <form action="/produk/cari" method="GET">
                                                <div class="input-group ">
                                                    <input type="text" name="cari" class="form-control pull-right" value="{{ old('cari') }}" placeholder="Search">
                                                    <div class="input-group-btn">
                                                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div> 
                                    </div>  
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body table-responsive no-padding">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Sub Kategori</th>
                                                <th>Nama</th>
                                                <th>Harga</th>
                                                <th>Gambar</th>
                                                <th>Deskripsi</th>
                                                <th width="15%">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                                @php
                                                    $no = 1;
                                                @endphp
                                                @foreach ($data_produk as $produk)
                                                <tr>
                                                    <td>{{ $no }}</td>
                                                    <td>{{ $produk->sub_kategori->nama }}</td>
                                                    <td>{{ $produk->nama }}</td>
                                                    <td>{{ $produk->harga }}</td>
                                                    <td>
                                                        <img  width="80px" height="50px" src="{{ asset('') }}storage/files/{{ $produk['gambar1'] }}"></img>
                                                        <img  width="80px" height="50px" src="{{ asset('') }}storage/files/{{ $produk['gambar2'] }}"></img>
                                                        <img  width="80px" height="50px" src="{{ asset('') }}storage/files/{{ $produk['gambar3'] }}"></img>
                                                        <img  width="80px" height="50px" src="{{ asset('') }}storage/files/{{ $produk['gambar4'] }}"></img>
                                                    </td>
                                                    <td>{!! substr($produk->deskripsi,0,100) !!}...</td>
                                                    <td>
                                                        <a href="{{ url('produk/edit', $produk->id_produk) }}" type="button" class="btn btn-success btn-sm"><i class="fa fa-edit"></i> Edit</a>
                                                        <a href="javascript:;" data-toggle="modal" onclick="deleteProduk({{$produk->id_produk}})"
                                                                data-target="#DeleteModal" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Delete</a>
                                                    </td>
                                                    @php
                                                        $no++;
                                                    @endphp
                                                </tr>
                                                @endforeach
                                        </tbody>
                                    </table>
                                    <br/>
                                </div>
                            </div>
                                    Halaman : {{ $data_produk->currentPage() }} <br/>
                                    Jumlah Data : {{ $data_produk->total() }} <br/>
                                    Data Per Halaman : {{ $data_produk->perPage() }} <br/>

                            {{ $data_produk->links() }}
                        </div>


@include('admin.modals.delete')

<script type="text/javascript">

    function deleteProduk(id_produk)
    {
        var id_produk = id_produk;
        var url = '{{ route("produk.destroy", ":id_produk") }}';
        url = url.replace(':id_produk', id_produk);
        $("#deleteForm").attr('action', url);
    }

    function formSubmit()
    {
        $("#deleteForm").submit();
    }
 </script>


@endsection
