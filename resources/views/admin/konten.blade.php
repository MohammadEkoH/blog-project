@extends('admin.app')

@section('title', 'Konten')

@section('content')
<div class="row">
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ url('dashboard') }}">Dashboard</a></li>
        <li class="active">Konten</li>
    </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
                <ul>
                        @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>

                    @if(session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
            <div class="col-md-12">
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li  class="active"><a href="{{  url('dashboard/konten') }}">Konten</a></li>
                    <li><a href="{{ url('dashboard/kontak') }}">Kontak</a></li>
                    <li><a href="{{ url('dashboard/tentang') }}">Tentang</a></li>
                    <li class="dropdown">
                </ul>
                <div class="tab-content">

                    <div class="tab-pane active">
                        <form enctype="multipart/form-data" action="{{ url('konten/store') }}" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token()}}">
                            <input type="hidden" name="id" value="{{ $id }}">
                            {{ method_field('post') }}
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputTitle">Title</label>
                                    <input value="{{ $title }}" type="text" class="form-control" name="title" id="exampleInputTitle" placeholder="Enter title">
                                    <div class="help-block with-errors">{{ $errors->first('deskripsi') }}</div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputFile">Logo</label><br>
                                    <img
                                    <?php
                                    if($logo == null ){
                                        ?>
                                        src="{{ asset('')}} /storage/files/100x100.png"
                                        <?php
                                    }else{
                                        ?>
                                        src="{{ asset('') }}storage/files/{{ $logo }}";
                                    <?php
                                    }
                                ?>
                                    id="showgambar" style="max-width:200px;max-height:200px;">
                                    <br> * Disarankan ukuran <i style="color:red">356x97 pixels </i>
                                    <input type="file" id="inputgambar" name="logo">
                                    <div class="help-block with-errors">{{ $errors->first('logo') }}</div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputFile">Background</label><br>
                                    <img
                                    <?php
                                        if($background == null ){
                                            ?>
                                            src="{{ asset('') }}assets/dist/img/background.jpeg"
                                            <?php
                                        }else{
                                            ?>
                                            src="{{ asset('') }}storage/files/{{ $background }}";
                                        <?php
                                        }
                                    ?>
                                     id="showBackground" style="max-width:200px;max-height:200px;">
                                     <br> * Disarankan ukuran <i style="color:red">1350x650 pixels</i>
                                    <input type="file" id="inputBackground" name="background">
                                    <div class="help-block with-errors">{{ $errors->first('background') }}</div>
                                </div>
                            </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary pull-right">Submit</button>
                            </div>
                        </form>
                    </div>


                <!-- /.tab-content -->
                </div>
                <!-- nav-tabs-custom -->
            </div>


<script type="text/javascript">


    $(document).ready(function() {

        $(".btn-success").click(function(){
            var html = $(".clone").html();
            $(".increment").after(html);
        });

        $("body").on("click",".btn-danger",function(){
            $(this).parents(".control-group").remove();
        });

    });

</script>

    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="{{asset('js/materialize.min.js')}}"></script>
    <script type="text/javascript">
        (function($){
            $(function(){

                $('.button-collapse').sideNav();

            }); // end of document ready
            })(jQuery); // end of jQuery name space
    </script>
    <script type="text/javascript">

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#showgambar').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#inputgambar").change(function () {
            readURL(this);
        });

        function readURL1(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#showBackground').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#inputBackground").change(function () {
            readURL1(this);
        });

    </script>
@endsection
