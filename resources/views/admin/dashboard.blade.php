@extends('admin.app')

@section('title', 'Dashboard')

@section('content')  
<div class="row">  
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li> 
        <li class="active">Dashboard</li>
    </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="callout callout-info">
        <h4>Selamat datang dihalaman Admin!</h4>

        <p>Add the layout-top-nav class to the body tag to get this layout. This feature can also be used with a
        sidebar! So use this class if you want to remove the custom dropdown menus from the navbar and use regular
        links instead.</p>
    </div> 
</div> 
@endsection 