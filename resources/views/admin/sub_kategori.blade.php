@extends('admin.app')

@section('title', 'Produk')

@section('content')
<div class="row">
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ url('dashboard') }}">Dashboard</a></li>
        <li class="active">Produk</li>
    </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
                <ul>
                    @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>

                @if(session('success'))
                  <div class="alert alert-success">
                      {{ session('success') }}
                  </div>
                @endif

                <style type="text/css">
                    .pagination li{
                        float: left;
                        list-style-type: none;
                        margin:5px;
                    }
                </style>
            <div class="col-md-12">
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                    <li><a href="{{ url('dashboard/produk') }}">Produk</a></li>
                    <li class="active"><a href="{{ url('dashboard/sub_kategori') }}">Sub Kategori</a></li>
                    <li><a href="{{ url('dashboard/kategori') }}">Kategori</a></li>
                    <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active">
                            <div class="box">
                                <div class="box-header">
                                    <div>
                                        <h3 class="box-title">
                                            <button type="button"  onclick="AddSubKategori()" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#sub-kategori-modal">
                                                <i class="fa fa-plus-square"></i><b> Tambah</b>
                                            </button>
                                        </h3><br><br>
                                        <div class="box-tools">
                                            <form action="/sub_kategori/cari" method="GET">
                                                <div class="input-group ">
                                                    <input type="text" name="cari" class="form-control pull-right" value="{{ old('cari') }}" placeholder="Search">
                                                    <div class="input-group-btn">
                                                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body table-responsive no-padding">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th width="10%">No</th>
                                                <th width="10%">Kategori</th>
                                                <th>Sub Kategori</th>
                                                <th width="15%">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                                @php
                                                    $no = 1;
                                                @endphp
                                                @foreach ($data_sub_kategori as $sub_kategori)
                                                <tr>
                                                    <td>{{ $no }}</td>
                                                    <td>{{ $sub_kategori->kategori->nama }}</td>
                                                    <td>{{ $sub_kategori->nama }}</td>
                                                    <td>
                                                        <button type="button" data-toggle="modal" data-target="#sub-kategori-modal" onclick="EditSubKategori('{{$sub_kategori->id_sub_kategori}}')" class="btn btn-success btn-sm"><i class="fa fa-edit"></i> Edit</button>

                                                        <a href="javascript:;" data-toggle="modal" onclick="deleteSubKategori({{$sub_kategori->id_sub_kategori}})"
                                                                data-target="#DeleteModal" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Delete</a>
                                                    </td>
                                                    @php
                                                        $no++;
                                                    @endphp
                                                </tr>
                                                @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                                    Halaman : {{ $data_sub_kategori->currentPage() }} <br/>
                                    Jumlah Data : {{ $data_sub_kategori->total() }} <br/>
                                    Data Per Halaman : {{ $data_sub_kategori->perPage() }} <br/>

                                    {{ $data_sub_kategori->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@include('admin.modals.sub_kategori')
@include('admin.modals.delete')
<script>
    /* Add Kategori */
    function AddSubKategori()
    {
        $('.form-sub-kategori')[0].reset();
        $('.sub-kategori-modal-title').text('Tambah Data');
        $('.form-sub-kategori').attr('action', url+"{{ url('sub_kategori/store')}}");
    }

    // Edit Kategori
    function EditSubKategori(id){
        $.ajax({
            url: '{{ url('api/sub_kategori') }}/'+id,
            datatype : 'JSON',
            success: function(data){
                $('.sub-kategori-modal-title').text('Edit Data');
                $('.form-sub-kategori').attr('action', '{{ url('sub_kategori/update') }}/'+id);
                for (var i = 0; i < data.length; i++) {
                    $('.nama').val( data[i].nama );
                    $('.kategori_id').val( data[i].kategori_id );
                }
            }
        });
    }

    function deleteSubKategori(id_sub_kategori)
    {
        var id_sub_kategori = id_sub_kategori;
        var url = '{{ route("sub_kategori.destroy", ":id_sub_kategori") }}';
        url = url.replace(':id_sub_kategori', id_sub_kategori);
        $("#deleteForm").attr('action', url);
    }

    function formSubmit()
    {
        $("#deleteForm").submit();
    }

</script>

@endsection
