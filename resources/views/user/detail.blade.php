@extends('user.app')

@section('title', 'Detail Produk')

@section('content')
<section class="page-header page-header-light page-header-more-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12"> 
                <ul class="breadcrumb breadcrumb-valign-mid">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Kategori</a></li>
                    <li class="active">Detail</li>
                </ul>
            </div>
        </div>
    </div>
</section> 
<div class="container"> 
    <div class="row pb-xl pt-md">
        <div class="col-md-9">
                    
            <div class="row">
                <div class="col-md-6">

                    <span class="thumb-info-listing-type thumb-info-listing-type-detail background-color-secondary text-uppercase text-color-light font-weight-semibold p-sm pl-md pr-md">
                        {{ $kategori }}
                    </span>

                    <div class="thumb-gallery">
                        <div class="lightbox" data-plugin-options="{'delegate': 'a', 'type': 'image', 'gallery': {'enabled': true}}">
                            <div class="owl-carousel owl-theme manual thumb-gallery-detail show-nav-hover" id="thumbGalleryDetail">
                                <div>
                                    <a href="{{ asset('') }}storage/files/{{ $gambar1 }}">
                                        <span class="thumb-info thumb-info-centered-info thumb-info-no-borders font-size-xl">
                                            <span class="thumb-info-wrapper font-size-xl">
                                                <img alt="Property Detail" src="{{ asset('') }}storage/files/{{ $gambar1 }}" class="img-responsive">
                                                <span class="thumb-info-title font-size-xl">
                                                    <span class="thumb-info-inner font-size-xl"><i class="icon-magnifier icons font-size-xl"></i></span>
                                                </span>
                                            </span>
                                        </span>
                                    </a>
                                </div>
                                <div>
                                    <a href="{{ asset('') }}storage/files/{{ $gambar2 }}">
                                        <span class="thumb-info thumb-info-centered-info thumb-info-no-borders font-size-xl">
                                            <span class="thumb-info-wrapper font-size-xl">
                                                <img alt="Property Detail" src="{{ asset('') }}storage/files/{{ $gambar2 }}" class="img-responsive">
                                                <span class="thumb-info-title font-size-xl">
                                                    <span class="thumb-info-inner font-size-xl"><i class="icon-magnifier icons font-size-xl"></i></span>
                                                </span>
                                            </span>
                                        </span>
                                    </a>
                                </div>
                                <div>
                                    <a href="{{ asset('') }}storage/files/{{ $gambar3 }}">
                                        <span class="thumb-info thumb-info-centered-info thumb-info-no-borders font-size-xl">
                                            <span class="thumb-info-wrapper font-size-xl">
                                                <img alt="Property Detail" src="{{ asset('') }}storage/files/{{ $gambar3 }}" class="img-responsive">
                                                <span class="thumb-info-title font-size-xl">
                                                    <span class="thumb-info-inner font-size-xl"><i class="icon-magnifier icons font-size-xl"></i></span>
                                                </span>
                                            </span>
                                        </span>
                                    </a>
                                </div>
                                <div>
                                    <a href="{{ asset('') }}storage/files/{{ $gambar4 }}">
                                        <span class="thumb-info thumb-info-centered-info thumb-info-no-borders font-size-xl">
                                            <span class="thumb-info-wrapper font-size-xl">
                                                <img alt="Property Detail" src="{{ asset('') }}storage/files/{{ $gambar4 }}" class="img-responsive">
                                                <span class="thumb-info-title font-size-xl">
                                                    <span class="thumb-info-inner font-size-xl"><i class="icon-magnifier icons font-size-xl"></i></span>
                                                </span>
                                            </span>
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="owl-carousel owl-theme manual thumb-gallery-thumbs mt" id="thumbGalleryThumbs">
                            <div>
                                <img alt="Property Detail" src="{{ asset('') }}storage/files/{{ $gambar1 }}" class="img-responsive cur-pointer">
                            </div>
                            <div>
                                <img alt="Property Detail" src="{{ asset('') }}storage/files/{{ $gambar2 }}" class="img-responsive cur-pointer">
                            </div>
                            <div>
                                <img alt="Property Detail" src="{{ asset('') }}storage/files/{{ $gambar3 }}" class="img-responsive cur-pointer">
                            </div>
                            <div>
                                <img alt="Property Detail" src="{{ asset('') }}storage/files/{{ $gambar4 }}" class="img-responsive cur-pointer">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-6">
                    
                    <table class="table table-striped">
                        <colgroup>
                            <col width="35%">
                            <col width="65%">
                        </colgroup>
                        <tbody>
                            <tr>
                                <td class="background-color-primary text-light pt-md">
                                    
                                </td>
                                <td class="font-size-xl font-weight-bold pt-sm pb-sm background-color-primary text-light">
                                    Detail Produk
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Nama
                                </td>
                                <td>
                                    {{ $nama }}
                                </td>
                            </tr> 
                            <tr>
                                <td>
                                    Daftar ID
                                </td>
                                <td>
                                    {{ $id }}
                                </td>
                            </tr> 
                            <tr>
                                <td>
                                    Harga
                                </td>
                                <td>
                                    @php 
                                        echo "Rp. " . number_format($harga, 0, ".", ".");
                                    @endphp
                                </td>
                            </tr> 
                            <tr>
                                <td>
                                    Sub Kategori
                                </td>
                                <td>
                                    <a style="color:blue"><u>{{ $sub_kategori }}</u> </a>
                                </td>
                            </tr> 
                        </tbody>
                    </table>

                </div>
            </div>

            <div class="row">
                <div class="col-md-12">

                    <h4 class="mt-md mb-md">Deskripsi</h4>

                    {!! $deskripsi !!}

                </div>
            </div>

        </div>
        <div class="col-md-3">
            <aside class="sidebar">  
                <div class="agents text-color-light center">
                    <h4 class="text-light pt-xl m-none">Produk Terbaru</h4>
                    <div class="owl-carousel owl-theme nav-bottom rounded-nav pl-xs pr-xs pt-md m-none" data-plugin-options="{'items': 1, 'loop': false, 'dots': false, 'nav': true}">
                        @foreach ($data_terbaru as $terbaru)
                            <div class="pr-sm pl-sm">
                                <a href="{{ url('u/detail?id=' . $terbaru->id_produk) }}" class="text-decoration-none">
                                    <span class="agent-thumb">
                                        <img class="img-responsive img-circle" src="{{ asset('') }}storage/files/{{ $terbaru->gambar1 }}" alt />
                                    </span>
                                    <span class="agent-infos text-light pt-md">
                                        <strong class="text-uppercase font-weight-bold">{{ $terbaru->nama }}</strong>
                                        <span class="font-weight-light">
                                            @php
                                                $harga = $terbaru->harga;
                                                echo "Rp. " . number_format($harga, 0, ".", ".");
                                            @endphp
                                        </span> 
                                    </span>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="newsletter box-shadow-custom p-xlg "> 
                    <h4 class="mt-xs mb-xs">
                        Sub Kategori
                    </h4>  
                    <table class="table table-striped">
                        <colgroup>
                            <col width="65%">
                            <col width="35%">
                        </colgroup>
                        <tbody> 
                            @foreach ($data_kategori as $row)
                                <tr>
                                    <td>
                                        {{ $row->sub_kategori->nama }}
                                    </td>
                                    <td>
                                        {{ $row->total }}
                                    </td>
                                </tr>  
                            @endforeach
                        </tbody>
                    </table>
                </div>    
            </aside>
        </div>
    </div> 
</div>
@endsection