@extends('user.app')

@section('title', 'Tentang')

@section('content')
    <section class="page-header page-header-light page-header-more-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Tentang</h1>
                    <ul class="breadcrumb breadcrumb-valign-mid">
                        <li><a href="demo-real-estate.html">Home</a></li> 
                        <li class="active">Tentang</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <div class="container">

        <div class="row pb-xl pt-md">
            <div class="col-md-9">
                
                {!! $deskripsi_konten['deskripsi'] !!}

            </div>
            <div class="col-md-3">
                <aside class="sidebar"> 
                    <div class="agents text-color-light center">
                        <h4 class="text-light pt-xl m-none">Produk Terbaru</h4>
                        <div class="owl-carousel owl-theme nav-bottom rounded-nav pl-xs pr-xs pt-md m-none" data-plugin-options="{'items': 1, 'loop': false, 'dots': false, 'nav': true}">
                            @foreach ($data_terbaru as $terbaru)
                                <div class="pr-sm pl-sm">
                                    <a href="{{ url('u/detail?id=' . $terbaru->id_produk) }}" class="text-decoration-none">
                                        <span class="agent-thumb">
                                            <img class="img-responsive img-circle" src="{{ asset('') }}storage/files/{{ $terbaru->gambar1 }}" alt />
                                        </span>
                                        <span class="agent-infos text-light pt-md">
                                            <strong class="text-uppercase font-weight-bold">{{ $terbaru->nama }}</strong>
                                            <span class="font-weight-light">
                                                @php
                                                    $harga = $terbaru->harga;
                                                    echo "Rp. " . number_format($harga, 0, ".", ".");
                                                @endphp
                                            </span> 
                                        </span>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="newsletter box-shadow-custom p-xlg "> 
                        <h4 class="mt-xs mb-xs">
                            Sub Kategori
                        </h4>  
                        <table class="table table-striped">
                            <colgroup>
                                <col width="65%">
                                <col width="35%">
                            </colgroup>
                            <tbody> 
                                @foreach ($data_kategori as $row)
                                    <tr>
                                        <td>
                                            {{ $row->sub_kategori->nama }}
                                        </td>
                                        <td>
                                            {{ $row->total }}
                                        </td>
                                    </tr>  
                                @endforeach
                            </tbody>
                        </table>
                    </div>   
                </aside>
            </div>
        </div>

    </div>
@endsection