<!DOCTYPE html>
<html>
	<head>

		<!-- Basic -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">	

		<title>{{ $title }} | @yield('title')</title>	

		<meta name="keywords" content="HTML5 Template" />
		<meta name="description" content="Porto - Responsive HTML5 Template">
		<meta name="author" content="okler.net">

		<!-- Favicon -->
		<link rel="shortcut icon" href="{{ asset('') }}assets/dist/img/eye.ico" type="image/x-icon" />
		<link rel="apple-touch-icon" href="img/apple-touch-icon.png">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="{{ asset('') }}assets_user/vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="{{ asset('') }}assets_user/vendor/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="{{ asset('') }}assets_user/vendor/animate/animate.min.css">
		<link rel="stylesheet" href="{{ asset('') }}assets_user/vendor/simple-line-icons/css/simple-line-icons.min.css">
		<link rel="stylesheet" href="{{ asset('') }}assets_user/vendor/owl.carousel/assets/owl.carousel.min.css">
		<link rel="stylesheet" href="{{ asset('') }}assets_user/vendor/owl.carousel/assets/owl.theme.default.min.css">
		<link rel="stylesheet" href="{{ asset('') }}assets_user/vendor/magnific-popup/magnific-popup.min.css">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="{{ asset('') }}assets_user/css/theme.css">
		<link rel="stylesheet" href="{{ asset('') }}assets_user/css/theme-elements.css">
		<link rel="stylesheet" href="{{ asset('') }}assets_user/css/theme-blog.css">
		<link rel="stylesheet" href="{{ asset('') }}assets_user/css/theme-shop.css">

		<!-- Current Page CSS -->
		<link rel="stylesheet" href="{{ asset('') }}assets_user/vendor/rs-plugin/css/settings.css">
		<link rel="stylesheet" href="{{ asset('') }}assets_user/vendor/rs-plugin/css/layers.css">
		<link rel="stylesheet" href="{{ asset('') }}assets_user/vendor/rs-plugin/css/navigation.css">

		<!-- Skin CSS -->
		<link rel="stylesheet" href="{{ asset('') }}assets_user/css/skins/skin-real-estate.css"> 

		<!-- Demo CSS -->
		<link rel="stylesheet" href="{{ asset('') }}assets_user/css/demos/demo-real-estate.css">

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="{{ asset('') }}assets_user/css/custom.css">

		<!-- Head Libs -->
		<script src="{{ asset('') }}assets_user/vendor/modernizr/modernizr.min.js"></script>

	</head>
	<body class="loading-overlay-showing" data-loading-overlay>
		<div class="loading-overlay">
			<div class="bounce-loader">
				<div class="bounce1"></div>
				<div class="bounce2"></div>
				<div class="bounce3"></div>
			</div>
		</div>

		<div class="body">
			<header id="header" class="header-narrow" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 37, 'stickySetTop': '-37px', 'stickyChangeLogo': false}">
				<div class="header-body background-color-primary pt-none pb-none">
					<div class="header-top header-top header-top-style-3 header-top-custom background-color-primary m-none">
						<div class="container">
							<nav class="header-nav-top pull-left">
								<ul class="nav nav-pills"> 
										<li class="hidden-xs">
											<span class="ws-nowrap"><i class="icon-location-pin icons" title="Alamat"></i> {{ $alamat }}</span>
										</li>
										<li>
											<span class="ws-nowrap"><i class="icon-call-out icons" title="Kontak"></i> {{ $nomor }}</span>
										</li>
										<li class="hidden-xs">
											<span class="ws-nowrap"><i class="icon-envelope-open icons"></i> <a class="text-decoration-none" href="mailto:{{ $email }}" title="Alamat Email">{{ $email }}</a></span>
										</li> 
								</ul>
							</nav>  
						</div>
					</div>
					<div class="header-container container custom-position-initial">
						<div class="header-row">
							<div class="header-column">
								<div class="header-logo">
									<a href="{{ url('/') }}">
										<img alt="logo" width="143" height="40" src="{{ asset('') }}storage/files/{{ $logo }}">
									</a>
								</div>
							</div>
							<div class="header-column">
								<div class="header-row">
									<div class="header-nav">
										<button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main">
											<i class="fa fa-bars"></i>
										</button>
										<div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1 collapse m-none">
											<nav>
												<ul class="nav nav-pills" id="mainNav">
                          <li class="dropdown-full-color dropdown-quaternary" style="background:rgb(46, 169, 222);">
                            <a href="{{ url('/') }}">
                              Home
                            </a>
													</li>
													<li class="dropdown dropdown-full-color dropdown-quaternary">
														<a class="dropdown-toggle">
															Kategori
														</a>
														<ul class="dropdown-menu">
															@foreach ($list_kategori as $row)
																<li><a  href="{{ url('u/kategori?id=' . $row->id_kategori) }}" > {{ $row->nama }} </a></li> 
															@endforeach
														</ul>
													</li> 
													<li class="dropdown-full-color dropdown-quaternary">
                            <a href="{{ url('u/kontak') }}">
                              Kontak
                            </a>
                          </li> 
													<li class="dropdown-full-color dropdown-quaternary">
														<a href="{{ url('u/tentang') }}">
															Tentang
														</a>
													</li> 
													<li class="dropdown dropdown-full-color dropdown-quaternary dropdown-mega" id="headerSearchProperties">
															<a class="dropdown-toggle" href="#">
																Pencarian <i class="fa fa-search"></i>
															</a>
															<ul class="dropdown-menu custom-fullwidth-dropdown-menu">
																<li>
																	<div class="dropdown-mega-content">
																		<form action="{{ url('u/p') }}" method="GET">
																			<div class="container p-none">
																				<div class="row">
																					<div class="col-md-10">
																						<div class="form-control-custom">
																							<input type="text" class="form-control col-md-12" name="cari" value="{{ old('cari') }}" placeholder="Cari Produk...">
																						</div>
																					</div> 
																					<div class="col-md-2">
																						<div class="form-control-custom">
																							<button type="submit" class="btn btn-secondary btn-lg btn-block text-uppercase font-size-sm"> Cari</button>
																						</div>
																					</div> 
																				</div>
																			</div>
																		</form>
																	</div>
																</li>
															</ul>
														</li>
												</ul>
											</nav>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>
			<div role="main" class="main">
				
				@yield('content')

				<footer id="footer" class="m-none custom-background-color-1"> 
					<div class="footer-copyright custom-background-color-1 pb-none">
						<div class="container">
							<div class="row pt-md pb-md">
								<div class="col-md-12 left m-none">
									<p>© Copyright 2017 <a href="{{ url('/dashboard') }}" style="color:rgb(46, 169, 222)">JIR</a>. All Rights Reserved.</p>
								</div>
							</div>
						</div>
					</div>
				</footer>
			</div>
		</div>

		<!-- Vendor -->
		<script src="{{ asset('') }}assets_user/vendor/jquery/jquery.min.js"></script>
		<script src="{{ asset('') }}assets_user/vendor/jquery.appear/jquery.appear.min.js"></script>
		<script src="{{ asset('') }}assets_user/vendor/jquery.easing/jquery.easing.min.js"></script>
		<script src="{{ asset('') }}assets_user/vendor/jquery-cookie/jquery-cookie.min.js"></script>
		<script src="{{ asset('') }}assets_user/vendor/bootstrap/js/bootstrap.min.js"></script>
		<script src="{{ asset('') }}assets_user/vendor/common/common.min.js"></script>
		<script src="{{ asset('') }}assets_user/vendor/jquery.validation/jquery.validation.min.js"></script>
		<script src="{{ asset('') }}assets_user/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<script src="{{ asset('') }}assets_user/vendor/jquery.gmap/jquery.gmap.min.js"></script>
		<script src="{{ asset('') }}assets_user/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
		<script src="{{ asset('') }}assets_user/vendor/isotope/jquery.isotope.min.js"></script>
		<script src="{{ asset('') }}assets_user/vendor/owl.carousel/owl.carousel.min.js"></script>
		<script src="{{ asset('') }}assets_user/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
		<script src="{{ asset('') }}assets_user/vendor/vide/vide.min.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="{{ asset('') }}assets_user/js/theme.js"></script>
		
		<!-- Current Page {{ asset('') }}assets_user/Vendor and Views -->
		<script src="{{ asset('') }}assets_user/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
		<script src="{{ asset('') }}assets_user/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

		<!-- Current Page {{ asset('') }}assets_user/Vendor and Views -->
		<script src="{{ asset('') }}assets_user/js/views/view.contact.js"></script>

		<!-- Demo -->
		<script src="{{ asset('') }}assets_user/js/demos/demo-real-estate.js"></script>
		
		<!-- Theme Custom -->
		<script src="{{ asset('') }}assets_user/js/custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="{{ asset('') }}assets_user/js/theme.init.js"></script>

    <script type="text/javascript">
      $(function() { 
          $('#mainNav a[href~="' + location.href + '"]').parents('li').addClass('active');  
      });
    </script>


		<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
			ga('create', 'UA-12345678-1', 'auto');
			ga('send', 'pageview');
		</script>
		 -->
 
	</body>
</html>
