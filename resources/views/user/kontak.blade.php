@extends('user.app')

@section('title', 'Kontak')

@section('content')
<section class="page-header page-header-light page-header-more-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Kontak</h1>
                    <ul class="breadcrumb breadcrumb-valign-mid">
                        <li><a href="demo-real-estate.html">Home</a></li>
                        <li class="active">Kontak</li>
                    </ul>
                </div>
            </div> 
        </div>
    </section>

    <div class="container">

        <div class="row">
            <div class="col-md-9">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>

                @if(session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif

                <style type="text/css">
                    .pagination li{
                        float: left;
                        list-style-type: none;
                        margin:5px;
                    }
                </style>
                <h4 class="heading-primary mt-lg">Kirim Pesan</h4>  

                <form action="{{ url('u/send') }}">
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-6">
                                <label>Nama *</label>
                                <input type="text" class="form-control" name="nama" placeholder="Amshar Salam" required>
                            </div>
                            <div class="col-md-6">
                                <label>Alamat Email *</label>
                                <input type="email" name="email" class="form-control" placeholder="example@mail.co.id" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-12">
                                <label>Pesan *</label>
                                <textarea class="form-control"  style="height:200px" name="pesan" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="submit" value="Kirim Pesan" class="btn btn-secondary mb-xlg" data-loading-text="Loading...">
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-3"> 
                <h4 class="heading-primary">Jam Kerja</h4>
                <ul class="list list-icons mt-md">
                    @foreach ($jam_kerja as $row)
                        {!! $row->jam_kerja !!} 
                    @endforeach 
                </ul> 
            </div> 
        </div> 
    </div>
 
@endsection