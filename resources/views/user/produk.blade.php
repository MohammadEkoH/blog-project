@extends('user.app')

@section('title', 'Produk')

@section('content')
<div class="container">
    <div class="row">
        <!-- Latest Posts -->
        <main class="posts-listing col-lg-8">
        <div class="container">
            <div class="row">
            <!-- post -->
            @foreach ($where_data_produk as $produk)
            <div class="post col-xl-6">
                <div class="post-thumbnail"><a href="post.html"><img src="{{ asset('') }}storage/files/{{ $produk->gambar }}" alt="..." class="img-fluid"></a></div>
                <div class="post-details">
                <div class="post-meta d-flex justify-content-between">
                    <div class="date meta-last">{{ Carbon\Carbon::parse($produk->created_at)->formatLocalized('%d %B | %Y')}}</div>
                    <div class="category"><a href="#">{{ $produk->kategori->nama }}</a></div>
                </div><a href="{{ url('detail') }}">
                    <h3 class="h4">{{ $produk->nama }}</h3></a>
                <p class="text-muted">{{ substr($produk->deskripsi, 0, 100) . '...' }} </p>
                <footer class="post-footer d-flex align-items-center"><a href="#" class="author d-flex align-items-center flex-wrap">
                    <div class="title"><span>{{ $produk->harga}}</span></div></a>

                    <div class="date"><i class="icon-clock"></i> {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $produk->updated_at)->diffForHumans() }}</div>
                </footer>
                </div>
            </div>
            @endforeach

            </div>
            <!-- Pagination -->
            <nav aria-label="Page navigation example">
            <ul class="pagination pagination-template d-flex justify-content-center">
                    {{ $where_data_produk->links() }}
            </ul>
            </nav>
        </div>
        </main>
        <aside class="col-lg-4">
        <!-- Widget [Search Bar Widget]-->
        <div class="widget search">
            <header>
            <h3 class="h6">Cari produk</h3>
            </header>
            <form action="#" class="search-form">
            <div class="form-group">
                <input type="search" placeholder="What are you looking for?">
                <button type="submit" class="submit"><i class="icon-search"></i></button>
            </div>
            </form>
        </div>
        <!-- Widget [Latest Posts Widget]        -->
        <div class="widget latest-posts">
            <header>
            <h3 class="h6">Latest Posts</h3>
            </header>
            @foreach ($data_terbaru as $terbaru)
                <div class="blog-posts"><a href="#">
                    <div class="item d-flex align-items-center">
                        <div class="image"><img src="\storage\files\{{ $terbaru->gambar}}" alt="..." class="img-fluid"></div>
                            <div class="title"><strong>{{ $terbaru->nama }}</strong>
                                <div class="d-flex align-items-center">
                                    <div class="views"><i class="icon-eye"></i> {{ $terbaru->harga }}</div>
                                    <div class="comments"><i class="icon-comment"></i>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $terbaru->updated_at)->diffForHumans() }}</div>
                                </div>
                            </div>
                        </div>
                </a></div>
            @endforeach

        </div>
        <!-- Widget [Categories Widget]-->
        <div class="widget categories">
            <header>
            <h3 class="h6">Categories</h3>
            </header>
                @foreach ($data_kategori as $data)
                <div class="item d-flex justify-content-between"><a href="#">{{ $data->kategori['nama']}}</a><span>{{$data->total}}</span></div>

                @endforeach

        </div>
        <!-- Widget [Tags Cloud Widget]-->
        <div class="widget tags">
            <header>
            <h3 class="h6">Tags</h3>
            </header>
            <ul class="list-inline">
            <li class="list-inline-item"><a href="#" class="tag">#Business</a></li>
            <li class="list-inline-item"><a href="#" class="tag">#Technology</a></li>
            <li class="list-inline-item"><a href="#" class="tag">#Fashion</a></li>
            <li class="list-inline-item"><a href="#" class="tag">#Sports</a></li>
            <li class="list-inline-item"><a href="#" class="tag">#Economy</a></li>
            </ul>
        </div>
        </aside>
    </div>
</div>
@endsection
