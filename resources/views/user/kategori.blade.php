@extends('user.app')

@section('title', 'Kategori')

@section('content') 
<section class="page-header page-header-light page-header-more-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12"> 
                <ul class="breadcrumb breadcrumb-valign-mid">
                    <li><a href="#">Home</a></li> 
                    <li class="active">Kategori</li>
                </ul>
            </div>
        </div>
    </div>
</section> 
<div class="container">
    <div class="row pt-xlg mt-xlg">
        <div class="col-md-9">
            <div class="row">
                <div class="col-md-8">
                    <h2 class="font-weight-normal mb-xs">
                        Daftar untuk <strong class="text-color-secondary font-weight-extra-bold">Kategori</strong>  
                    </h2> 
                </div>
                {{-- <div class="col-md-4">  
                    <div class="pull-right"> 
                        <form action="#" method="GET">
                            <input type="text"  name="cari" placeholder="Cari kategori" value="{{ old('cari') }}">
                            <button class="btn btn-primary">cari</button>
                        </form>
                    </div>
                </div>  --}}
            </div>
            <div class="row">
                <ul id="listingLoadMoreWrapper" class="properties-listing sort-destination p-none" data-total-pages="2">
                    @foreach ($where_data_kategori as $kategori)
                    <li class="col-md-3 col-sm-6 col-xs-12 p-md isotope-item">
                            <div class="listing-item">
                                <a href="{{ url('u/detail?id=' . $kategori->id_produk) }}" class="text-decoration-none">
                                    <span class="thumb-info thumb-info-lighten">
                                        <span class="thumb-info-wrapper m-none">
                                            <img src="{{ asset('') }}storage/files/{{ $kategori->gambar1 }}" class="img-responsive" alt="">
                                            <span class="thumb-info-listing-type background-color-secondary text-uppercase text-color-light font-weight-semibold p-xs pl-md pr-md">
                                                {{ $kategori->sub_kategori->kategori->nama }}
                                            </span>
                                        </span>
                                        <span class="thumb-info-price background-color-primary text-color-light text-lg p-sm pl-md pr-md">
                                            @php
                                                $harga = $kategori->harga;
                                                echo "Rp. " . number_format($harga, 0, ".", ".");
                                            @endphp
                                            <i class="fa fa-caret-right text-color-secondary pull-right"></i>
                                        </span>
                                        <span class="custom-thumb-info-title b-normal p-lg">
                                            <span class="thumb-info-inner text-md"> {{ $kategori->nama }} </span>
                                            <ul class="accommodations text-uppercase font-weight-bold p-none text-sm">
                                                <li>
                                                    <span class="accomodation-title">
                                                        Beds:
                                                    </span>
                                                    <span class="accomodation-value custom-color-1">
                                                        3
                                                    </span>
                                                </li>
                                                <li>
                                                    <span class="accomodation-title">
                                                        Baths:
                                                    </span>
                                                    <span class="accomodation-value custom-color-1">
                                                        2
                                                    </span>
                                                </li>
                                                <li>
                                                    <span class="accomodation-title">
                                                        Sq Ft:
                                                    </span>
                                                    <span class="accomodation-value custom-color-1">
                                                        500
                                                    </span>
                                                </li>
                                            </ul>
                                        </span>
                                    </span>
                                </a>
                            </div>
                        </li> 
                    @endforeach
                </ul>
                <div class="row mt-lg mb-xlg">
                    <div class="col-md-12 center">
                        <ul class="pagination">
                            <form action="">
                                {{ $where_data_kategori->links() }}  
                            </form>
                        </ul>
                    </div>
                </div>
            </div> 
        </div>
        <div class="col-md-3">
            <aside class="sidebar"> 
                <div class="agents text-color-light center">
                    <h4 class="text-light pt-xl m-none">Produk Terbaru</h4>
                    <div class="owl-carousel owl-theme nav-bottom rounded-nav pl-xs pr-xs pt-md m-none" data-plugin-options="{'items': 1, 'loop': false, 'dots': false, 'nav': true}">
                        @foreach ($data_terbaru as $terbaru)
                            <div class="pr-sm pl-sm">
                                <a href="{{ url('u/detail?id=' . $terbaru->id_produk) }}" class="text-decoration-none">
                                    <span class="agent-thumb">
                                        <img class="img-responsive img-circle" src="{{ asset('') }}storage/files/{{ $terbaru->gambar1 }}" alt />
                                    </span>
                                    <span class="agent-infos text-light pt-md">
                                        <strong class="text-uppercase font-weight-bold">{{ $terbaru->nama }}</strong>
                                        <span class="font-weight-light">
                                            @php
                                                $harga = $terbaru->harga;
                                                echo "Rp. " . number_format($harga, 0, ".", ".");
                                            @endphp
                                        </span> 
                                    </span>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="newsletter box-shadow-custom p-xlg "> 
                    <h4 class="mt-xs mb-xs">
                        Sub Kategori
                    </h4>  
                    <table class="table table-striped">
                        <colgroup>
                            <col width="65%">
                            <col width="35%">
                        </colgroup>
                        <tbody> 
                            @foreach ($data_kategori as $row)
                                <tr>
                                    <td>
                                        {{ $row->sub_kategori->nama }}
                                    </td>
                                    <td>
                                        {{ $row->total }}
                                    </td>
                                </tr>  
                            @endforeach
                        </tbody>
                    </table>
                </div>   
            </aside>
        </div>
    </div>
</div>
@endsection

