@extends('user.app')

@section('title', 'Home')

@section('content')
<div class="slider-container light rev_slider_wrapper" style="height: 650px;">
        <div id="revolutionSlider" class="slider rev_slider" data-plugin-revolution-slider data-plugin-options="{'delay': 9000, 'gridwidth': 1170, 'gridheight': 650, 'disableProgressBar': 'on', 'navigation': {'arrows': {'enable': true, 'left':{'container':'slider','h_align':'right','v_align':'center','h_offset':20,'v_offset':-80},'right':{'container':'slider','h_align':'right','v_align':'center','h_offset':20,'v_offset':80}}}}">
            <div class="slides-number hidden-xs">
                <span class="atual">1</span>
                <span class="total">3</span>
            </div>
            <img src="{{ asset('') }}storage/files/{{ $background }}"  
                alt=""
                data-bgposition="center center" 
                data-bgfit="cover" 
                data-bgrepeat="no-repeat"
                class="rev-slidebg"
                width="1350px"
                height="650px">
            <ul>  
                @foreach ($data_terbaru as $terbaru) 
                    <li data-transition="fade">  
                        <div class="tp-caption tp-shape tp-shapewrapper tp-resizeme skrollable skrollable-after" 
                            id="slide-529-layer-1" 
                            data-x="left" data-hoffset="15"
                            data-y="center" data-voffset="0"
                            data-width="360" 
                            data-height="360" 
                            data-whitespace="nowrap" 
                            data-transform_idle="o:1;" 
                            data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeOut;" 
                            data-transform_out="x:left;s:1200;e:Power3.easeInOut;" 
                            data-start="500" 
                            data-responsive_offset="on" 
                            style="background-color: rgb(255, 255, 255); padding: 30px; overflow: hidden;">
                                <span class="featured-border" style="border: 2px solid #dcdde0; width: 90%; position: absolute; height: 90%; top: 5%; left: 5%;"></span>
                                <span class="feature-tag" data-width="50" data-height="50" style="background: #2bca6e; color: #FFF; text-transform: uppercase; padding: 15px 102px; position: absolute; right: -24%; top: 6%; -webkit-transform: rotate(45deg); -moz-transform: rotate(45deg); -ms-transform: rotate(45deg); -o-transform: rotate(45deg); transform: rotate(45deg);">
                                    TERBARU
                                </span>
                            </div>

                        <div class="tp-caption main-label"
                            data-x="left" data-hoffset="35"
                            data-y="center" data-voffset="-50"
                            data-start="1500"
                            data-whitespace="nowrap"						 
                            data-transform_in="y:[-100%];s:500;"
                            data-transform_out="opacity:0;s:500;"
                            data-textAlign="center" 
                            style="z-index: 5; font-size: 1.9em; color: #000; text-transform: uppercase; font-weight: 900; text-shadow: none; width: 27vw; max-width: 320px;"
                            data-mask_in="x:0px;y:0px;">{{ $terbaru->nama }}</div>

                        <div class="tp-caption"
                            data-x="left" data-hoffset="35"
                            data-y="center" data-voffset="0"
                            data-start="1500"
                            data-height="44"
                            data-whitespace="nowrap"						 
                            data-transform_idle="o:1;" 
                            data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeOut;" 
                            data-transform_out="x:left;s:1200;e:Power3.easeInOut;"
                            data-textAlign="center" 
                            style="z-index: 5; font-size: 3em; font-weight: 400; text-transform: uppercase; color: #219cd2; line-height: 0.8em; width: 27vw; max-width: 320px;"
                            data-mask_in="x:0px;y:0px;">
                            @php
                                $harga = $terbaru->harga;
                                echo "Rp. " . number_format($harga, 0, ".", ".");
                            @endphp
                        </div>

                        <a class="tp-caption slide-button"
                            href="{{ url('u/detail?id=' . $terbaru->id_produk) }}" 
                            data-x="left" data-hoffset="108"
                            data-y="center" data-voffset="60"
                            data-start="1500"
                            data-whitespace="nowrap"						 
                            data-transform_in="y:[100%];s:500;"
                            data-transform_out="opacity:0;s:500;"
                            style="z-index: 5; font-size: 1em; text-transform: uppercase; background: #219cd2; padding: 12px 35px; color: #FFF;"
                            data-mask_in="x:0px;y:0px;">DETAIL</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
    @php
        $id_filterAir   = "";
        $id_keripik     = "";
        $id_abon        = "";
    @endphp
    <div class="container">
        <div class="row pt-xlg mt-xlg">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="font-weight-normal mb-xs">
                            Daftar semua <strong class="text-color-secondary font-weight-extra-bold">Produk</strong>  
                        </h2> 
                    </div>
                    {{-- <div class="col-md-4">  
                        <div class="pull-right"> 
                            <form  action="{{ url('u/p') }}" method="GET">
                                <input type="text"  name="cari" placeholder="Cari produk" value="{{ old('cari') }}">
                                <button class="btn btn-primary">cari</button>
                            </form>
                        </div>
                    </div>  --}}
                </div>
                <div class="row">
                    <ul id="listingLoadMoreWrapper" class="properties-listing sort-destination p-none" data-total-pages="2">
                        @foreach ($data_produk as $terbaru)
                        <li class="col-md-3 col-sm-6 col-xs-12 p-md isotope-item">
                                <div class="listing-item">
                                    <a href="{{ url('u/detail?id=' . $terbaru->id_produk) }}" class="text-decoration-none">
                                        <span class="thumb-info thumb-info-lighten">
                                            <span class="thumb-info-wrapper m-none">
                                                <img src="{{ asset('') }}storage/files/{{ $terbaru->gambar1 }}" class="img-responsive" alt="">
                                                <span class="thumb-info-listing-type background-color-secondary text-uppercase text-color-light font-weight-semibold p-xs pl-md pr-md">
                                                    {{ $terbaru->sub_kategori->kategori->nama }}
                                                </span>
                                            </span>
                                            <span class="thumb-info-price background-color-primary text-color-light text-lg p-sm pl-md pr-md">
                                                @php
                                                    $harga = $terbaru->harga;
                                                    echo "Rp. " . number_format($harga, 0, ".", ".");
                                                @endphp
                                                <i class="fa fa-caret-right text-color-secondary pull-right"></i>
                                            </span>
                                            <span class="custom-thumb-info-title b-normal p-lg">
                                                <span class="thumb-info-inner text-md"> {{ $terbaru->nama }} </span>
                                                <ul class="accommodations text-uppercase font-weight-bold p-none text-sm">
                                                    <li>
                                                        <span class="accomodation-title">
                                                            Beds:
                                                        </span>
                                                        <span class="accomodation-value custom-color-1">
                                                            3
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <span class="accomodation-title">
                                                            Baths:
                                                        </span>
                                                        <span class="accomodation-value custom-color-1">
                                                            2
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <span class="accomodation-title">
                                                            Sq Ft:
                                                        </span>
                                                        <span class="accomodation-value custom-color-1">
                                                            500
                                                        </span>
                                                    </li>
                                                </ul>
                                            </span>
                                        </span>
                                    </a>
                                </div>
                            </li> 
                        @endforeach
                    </ul>
                    <div class="row mt-lg mb-xlg">
                        <div class="col-md-12 center">
                            <ul class="pagination">
                                {{ $data_produk->links() }} 
                            </ul>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </div>
@endsection

